import mysql.connector

#Setting up database
db = mysql.connector.connect(
    host="localhost",
    user="username",
    passwd="password",
    database="dnddb",
    auth_plugin='mysql_native_password'
)
cursor = db.cursor()

#Setting up variables
max_mana = 1000
curr_mana = max_mana

#Main Program
while True:
    print(f"Mana: {curr_mana} / {max_mana}")
    cast_spell = input()
    if cast_spell == "Quit" or "Q":
        break
    try:
        cursor.execute(f"SELECT * FROM dnd5_spells WHERE spell_name='{cast_spell}'")
        spell = cursor.fetchone()
        mana_cost = 3 ** spell[2]
        print(f"You casted {spell[1]} a {spell[3]} spell for {mana_cost} mana!") #spell[2] = spell level int
        curr_mana -= mana_cost
    except:
        print("Spell doesn't exist in the database. Please check the spelling(no pun intended) and try again.")

